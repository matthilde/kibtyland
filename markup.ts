/*
  Some basic markup language that compiles to HTML.
  Will be used for posting stuff as markdown offers stuff that I
  don't want in there.
*/
import { XmlEntities } from "https://deno.land/x/html_entities@v1.0/mod.js";

const autolink = /((([A-Za-z]{3,9}:(?:\/\/)?)(?:[\-;:&=\+\$,\w]+@)?[A-Za-z0-9\.\-]+|(?:www\.|[\-;:&=\+\$,\w]+@)[A-Za-z0-9\.\-]+)((?:\/[\+~%\/\.\w\-_]*)?\??(?:[\-\+=&;%@\.\w_]*)#?(?:[\.\!\/\\\w]*))?)/gim;
const quote = /^(&gt;.*)$/gim;
const linebreak = /$/gim;

export function renderHTML(src: string): string {
	const sanitizedHTML = XmlEntities.encode(src);
	const result = sanitizedHTML
		.replace(autolink, '<a href="$1">$1</a>')
		.replace(quote,    '<quote>$1</quote>')
		.replace(linebreak,'<br />');

	return result;
}

#!/usr/bin/env bash
#
# Database initialization script
#

[ "$1" = "-f" ] && rm kibty.db
[ -f "kibty.db" ] && {
	echo "Database exists! Exiting..."
	exit 1
}

cat <<EOF | sqlite3 kibty.db
-- SQL database generation script

-- The password is an SHA-256 digest hex string for obvious reasons
-- If it has been invited by someone, invited_by is filled.
CREATE TABLE users (
    id       INTEGER     PRIMARY KEY AUTOINCREMENT,
	name     VARCHAR(16) NOT NULL UNIQUE,
	password CHAR(64)    NOT NULL,
	bio      TEXT        DEFAULT 'I am kibty user',

	invited_by INTEGER,
	invites  INTEGER     DEFAULT 2,
	banned   BOOL        DEFAULT FALSE,
	admin    BOOL        DEFAULT FALSE
);

-- all posts, no matter the board, are here.
-- a post can also contain an image.
-- If it has been posted using the API, fromapi is TRUE
CREATE TABLE posts (
    id      INTEGER PRIMARY KEY AUTOINCREMENT,
	board   TEXT	NOT NULL,
	author  INTEGER NOT NULL,
	stamp   INTEGER NOT NULL,
    content TEXT    NOT NULL,
    img     TEXT,

    reply   BOOL    DEFAULT FALSE,
    replyid INTEGER,
    pinned  BOOL    DEFAULT FALSE,
	fromapi BOOL	DEFAULT FALSE,
	deleted BOOL    DEFAULT FALSE
);

-- API keys
CREATE TABLE keys (
    token	TEXT    PRIMARY KEY,
    user    INTEGER NOT NULL,
    read    BOOL    DEFAULT FALSE,
    write   BOOL    DEFAULT FALSE
);
-- Invitation links
CREATE TABLE invites (
	code    CHAR(32) PRIMARY KEY,
    inviter INTEGER  DEFAULT NULL
);

EOF

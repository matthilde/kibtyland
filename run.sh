#!/bin/sh

[ -d img ] || mkdir img
[ -d logs ] || mkdir logs
[ -f kibty.db ] || ./init-database

deno run --allow-net --allow-write --allow-read main.ts

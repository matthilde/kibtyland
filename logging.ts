// Small logging system :)
import {
	red, yellow, green, blue, bgRed, white,
	bold, reset
} from "https://deno.land/std@0.146.0/fmt/colors.ts";
import { format } from "https://deno.land/std@0.146.0/datetime/mod.ts";

const logRoot = "./logs/";
const logEncoder = new TextEncoder();
const logFile = await Deno.open(logRoot + genLogFile(),
								{ write: true, create: true });
	
function getUTC() {
	return format(new Date(), "[dd/MM/yyyy HH:mm:ss]");
}
function genLogFile() {
	return format(new Date(), "yyyyMMdd.HHmmss.log");
}
function logNWrite(prefix, color, message, msgwrapper = x => x) {
	const p   = `[${prefix}]${getUTC()}`;
	const msg = `${p} ${message}`;
	const rmsg = `${bold(color(p))} ${msgwrapper(message)}`;
	console.log(rmsg);
	logFile.writeSync(logEncoder.encode(msg + '\n'));
}

export function info(message: string): void {
	logNWrite('i', green, message);
}
export function log(message: string): void {
	logNWrite('#', blue, message);
}
export function warn(message: string): void {
	logNWrite('!', yello, message, bold);
}
export function error(message: string): void {
	logNWrite('ERROR', x => bgRed(white(x)), message, bold);
}

#!/usr/bin/env bash
# Creates an admin account

[ -z $1 ] && {
	echo "Usage: create-admin USERNAME"
	exit 1
}

gen_digest () {
    set -- $(printf "%s%s" "$(< serversalt.txt)" "$1" | sha256sum -)
    printf "%s" "$1"
}

username="$1"
printf "Password: "
read -s password
checksum=$(gen_digest "$password")

cat <<EOF | sqlite3 kibty.db
INSERT INTO users (name, password, admin) VALUES (
    '${username}', '${checksum}', TRUE);
EOF

/*
  Kibtyland

  An image board service by matthilde.
  Made to try out Deno and TypeScript.
  */
import { Application,
		 Router,
	     Status,
		 REDIRECT_BACK,
	     HttpError } from "https://deno.land/x/oak@v10.6.0/mod.ts";
import { renderFile,
	     renderFileToString } from "https://deno.land/x/dejs/mod.ts";
import { format } from "https://deno.land/std@0.146.0/datetime/mod.ts";
import * as utils from "./imageboard.ts";
import * as log   from "./logging.ts";
import config from "./config.json" assert { type: "json" };

import { KibtyUser,
		 KibtyBoard,
		 KibtyQuery,
		 KibtyPost } from "./imageboard.ts";

const { cwd } = Deno;

const PORT = 8080;
const templates = `${cwd()}/views/`;
const router = new Router();
const app    = new Application({keys: [utils.randomString()]});

// Check for server salt
// TODO: Server salt thing.

log.info("ki/b/tyland by matthilde! - https://matthil.de");
log.info("Copyleft (C) 2022 Matthilde Autumn Maelle");
log.info("Licensed under the GNU GPLv3");

const renderFromBaseStr = async (ctx, content: string, params): string =>
	await renderFile(templates + "base.ejs",
					 Object.assign({ webcontent: content,
									 featured: config.featuredBoards,
									 loginuser: await ctx.cookies.get('loggedUser'),
								     }, params));
const renderFromBase = async (ctx, filename: string,
							  params = {},
							  baseparams = {}): string => {
	const content = await renderFileToString(templates + filename,
									         params);
	return await renderFromBaseStr(ctx, content, params);
}

router.get("/(static|img)/:filename", async (ctx) => {
	await ctx.send({
		root: `${cwd()}`,
		index: "index.html"
	});
});
router.all("/", async (ctx) => {
	const loggedUser: string = await ctx.cookies.get("loggedUser");
	const user = loggedUser ? new KibtyUser(loggedUser) : null;
	ctx.response.body = await renderFromBase(ctx, "index.ejs",
											 { loggedUser: user,
											   format: utils.TimestampToString,
											   news: utils.getLatestNews(),
											   boards: config.boards });
});
router.get("/about", async (ctx) => {
	ctx.response.body = await renderFromBase(ctx, "about.ejs");
});

// Login/Register
router.all("/register", async (ctx, next) => {
	let error: string = "";
	const messages = {
		creds:   "Please enter a proper username and password.",
		exists:  "This account already exists.",
		code:    "Invalid invitation code.",
		success: "Account creation successful!"
	}
	switch (ctx.request.method) {
		case "POST":
			const body = await ctx.request.body({type: 'form'});
			const data = await body.value;
			const success = utils.registerAccount(
				data.get('username'),
				data.get('password'),
				data.get('verif'));

			error = messages[success];
		case "GET":
			ctx.response.body = await renderFromBase(ctx, "register.ejs",
													 { error,
													   uregex: utils.uregex,
													   pregex: utils.pregex });
			break;
		default: ctx.throw(400); break;
	}
});
router.all("/login", async (ctx) => {
	if (await ctx.cookies.get("loggedUser")) {
		ctx.response.body = await renderFromBaseStr(ctx, 
			"You already are logged in.");
		return;
	}
	let error: string = null;
	const messages = {
		success: "Successfully logged in!",
		creds:   "Invalid credentials.",
		banned:  "This account is banned."
	}
	switch (ctx.request.method) {
		case "POST":
			const body = await ctx.request.body({type: 'form'});
			const data = await body.value;
			const success = utils.loginAccount(
				data.get('username'),
				data.get('password'))
			error = messages[success];
			if (success == 'success') {
				await ctx.cookies.set("loggedUser", data.get('username'));
			}
		case "GET":
			ctx.response.body = await renderFromBase(ctx, "login.ejs",
													{ error });
			break;
		default: ctx.throw(400); break;
	}
});
router.get("/logoff", async (ctx) => {
	let message = "You are not logged in.";
	if (await ctx.cookies.get("loggedUser")) {
		await ctx.cookies.set("loggedUser", null);
		message = "You are now logged off.";
	}
	ctx.response.body = await renderFromBaseStr(ctx, message);
});
// Admin stuff
router.all("/admin/:command([a-z]*)(\/?):arg([A-z0-9\-]*)", async (ctx) => {
	const username = await ctx.cookies.get("loggedUser");
	if (!username || !utils.isUserAdmin(username)) {
		ctx.response.body = await renderFromBaseStr(ctx, 
			"<h1>403</h1>wtf are you doing here??? gtfo smh");
		ctx.response.status = 403;
	} else if (ctx.request.method == "GET") {
		switch (ctx.params.command) {
			case 'createInvite':
				utils.adminCreateInvite();
				break;
			case 'revokeInvite':
				utils.adminRevokeInvite(ctx.params.arg);
				break;
			case 'dashboard':
				ctx.response.body = await renderFromBase(ctx, "admin.ejs",
														 { invites: utils.listInvites() });
				break;
		}
		if (ctx.params.command != 'dashboard')
			await ctx.response.redirect("/admin/dashboard");
	} else if (ctx.request.method == "POST") {
		const body = await ctx.request.body({type: 'form'});
		const data = await body.value;

		await ctx.response.redirect("/admin/dashboard");
	}
});


//////////////////////////////////////////////////////////////////////
////// BOARD STUFF
//////////////////////////////////////////////////////////////////////

router.all("/b/:board([A-z0-9]{1,8})(\/?):id([0-9]*)", async (ctx) => {
	const boardID = utils.searchJSON(config.boards, "name", ctx.params.board);
	if (boardID == -1) ctx.throw(404);

	const error_messages = {
		'message': 'Invalid message',
		'image':   'Please upload a valid PNG or JPEG image'
	}
	let error = null;
	const username = await ctx.cookies.get("loggedUser");
	const board = config.boards[boardID];
	let prevmsg = null, invalidreq = false;
	switch (ctx.request.method) {
		case 'POST':
			if (!username) {
				ctx.response.body = await renderFromBaseStr(ctx, "please log in");
				return;
			} else if (board.ro && !utils.isUserAdmin(username)) {
				error = "You need to be admin to post.";
			} else {
				invalidreq = true;
				const body = await ctx.request.body({type: 'form-data'});
				const data = await body.value.read({
					maxFileSize: Math.floor(1048576 * config.posts.maxUpload)
				});
				error = await utils.userPost(username, data,
											 ctx.params.board);
				error = error == 'success' ?
					null : error_messages[error];
				prevmsg = data.fields['message'];
			}

			if (!error) {
				await ctx.response.redirect("/b/" + ctx.params.board);
				break;
			}
		case 'GET':
			const page = ctx.params.id == '' ? 0 : parseInt(ctx.params.id);
			const posts = utils.getTopPosts(ctx.params.board, page);

			const user = username ?
				new KibtyUser(username) : null;

			ctx.response.body = await renderFromBase(ctx, "board.ejs", {
				pageno: page,
				query:  posts,
				board,
				format: utils.TimestampToString,
				postPolicy: config.posts,
				user, prevmsg, error
			});
			break;
		default: ctx.throw(400); break;
	}
})
router.all("/p/:id([0-9]+)", async (ctx) => {
	const post = utils.getPostById(ctx.params.id, true);
	if (!post) await ctx.throw(404);
	if (post.reply)
		await ctx.response.redirect('/p/' + post.replyid);

	const error_messages = {
		'message': 'Invalid message (must be between 1 and 500 chars)',
		'image':   'Please upload a valid PNG or JPEG image',
		'deleted': 'This post has been "hidden", and is now readonly'
	}
	let error = null;
	const username = await ctx.cookies.get("loggedUser");
	let prevmsg = null;
	switch (ctx.request.method) {
		case 'POST':
			if (!username) {
				ctx.response.body = await renderFromBaseStr(ctx, "please log in dumbass");
				return;
			} else if (post.deleted) {
				error = 'deleted';
			} else {
				const body = await ctx.request.body({type: 'form-data'});
				const data = await body.value.read({
					maxFileSize: Math.floor(1048576 * config.posts.maxUpload)
				});
				error = await utils.userPost(username, data,
											 post.board,
											 ctx.params.id);
				error = error == 'success' ?
					null : error_messages[error];

				prevmsg = data.fields['message'];
			}

			if (!error) {
				await ctx.response.redirect('/p/' + post.id);
				break;
			}
		case 'GET':
			const posts = new KibtyQuery([post]);
			const user = username ?
				new KibtyUser(username) : null;

			ctx.response.body = await renderFromBase(ctx, "viewpost.ejs", {
				query:  posts,
				deleted: post.deleted,
				format: utils.TimestampToString,
				postid: ctx.params.id,
				postPolicy: config.posts,
				user, prevmsg, error
			});
			break;
		default: ctx.throw(400); break;
	}
})
router.get("/p/:id/delete", async (ctx) => {
	const post = utils.getPostById(ctx.params.id, true);
	const username = await ctx.cookies.get("loggedUser");
	if (!username) await ctx.throw(403);

	const user = new KibtyUser(username);
	if (user.id != post.author && !user.admin)
		await ctx.throw(403);
	
	if (!post) await ctx.throw(404);
	const prevURL = ctx.request.headers.get('referer');

	utils.deletePost(post.id);

	if (prevURL) ctx.response.redirect(prevURL);
	else ctx.response.body = "success";
});

router.all("/u/:username", async (ctx) => {
	const uid = utils.getUserID(ctx.params.username);
	if (!uid) await ctx.throw(404);
	const username = await ctx.cookies.get('loggedUser');
	const loggedUser = username ? new KibtyUser(username) : null;
	const user = new KibtyUser(uid);

	let error = null;
	const error_messages = {
		size: "You bio is too large.",
		success: null
	}
	switch (ctx.request.method) {
		case 'POST':
			if (!loggedUser || user.id != loggedUser.id) ctx.throw(403);
			const body = await ctx.request.body({ type: 'form-data' });
			const data = await body.value.read({
				maxFileSize: 0
			});
			const bio  = data.fields['bio'] ? data.fields['bio'] : "";

			error = utils.changeUserBio(user.id, bio);
			error = error_messages[error];
			if (!error)
				await ctx.response.redirect(`/u/${ctx.params.username}`);
		case 'GET':
			ctx.response.body = await renderFromBase(ctx, "user.ejs", {
				error,
				loggedUser, user,
				postPolicy: config.posts,
				query: utils.getUserLatestPosts(uid),
				format: utils.TimestampToString });
			break;
	}
});
router.get("/u/:username/ban", async (ctx) => {
	const username = await ctx.cookies.get('loggedUser');
	if (!username || !utils.isUserAdmin(username))
		await ctx.throw(403);

	const targetUID = utils.getUserID(ctx.params.username);
	if (!targetUID) await ctx.throw(404);

	const targetUser = new KibtyUser(targetUID);
	utils.adminBanUser(targetUser.name, !targetUser.banned);

	await ctx.response.redirect(`/u/${ctx.params.username}`);
});

// Error handling
app.use(async (ctx, next) => {
	try {
		await next();
	} catch (e) {
		let errbody;
		if (e instanceof HttpError) {
			ctx.response.status = e.status;
			errbody = `<h1>Error ${e.status}</h1><p><b>`;
			errbody += e.expose ? Status[e.status] : e.message;
			errbody += "</b>";

			ctx.response.body = await renderFromBaseStr(ctx, errbody);
		} else if (e instanceof Error) {
			ctx.response.status = 500;
			errbody = `<h1>Internal Server Error</h1>`;
			ctx.response.body = await renderFromBaseStr(ctx, errbody);

			log.error(e.stack);
			log.error("INTERNAL SERVER ERROR!");
		}
	}

	if (ctx.response.status != 304) {
		const loggedUser = await ctx.cookies.get("loggedUser");
		const username   = loggedUser ? ` (${loggedUser})` : '';
		log.log(`${ctx.response.status} ${ctx.request.url.pathname} - ` +
			`${ctx.request.ip}${username}`);
	}
})
// Verify if user is banned
app.use(async (ctx, next) => {
	const username = await ctx.cookies.get("loggedUser");
	if (username && utils.isUserBanned(username)) {
		await ctx.cookies.set('loggedUser', null);
	}
	await next();
});
// Router stuff
app.use(router.routes());
app.use(router.allowedMethods());
app.use(async (ctx) => {
	ctx.response.body = await renderFromBaseStr(ctx, "<h1>404</h1>");
	ctx.response.status = 404;
});

app.addEventListener("listen", ({ hostname, port, serverType }) => {
	log.log(`HTTP Server listening on ${hostname}:${port}`);
});

app.listen({ hostname: config.host.hostname,
			 port:     config.host.port });

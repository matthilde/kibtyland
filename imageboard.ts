// Functions for image board database, login and other shit
import { DB } from "https://deno.land/x/sqlite/mod.ts";
import { crypto } from "https://deno.land/std@0.146.0/crypto/mod.ts";
import { exists, copy } from "https://deno.land/std/fs/mod.ts";
import config from "./config.json" assert { type: "json" };
import * as markup from "./markup.ts";
const { cwd } = Deno;

const db = new DB(`${cwd()}/kibty.db`);
const imgDir = `${cwd()}/img/`;
const verifchars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
let verifstring: string = null;

// sever salt
// You can put anything as a server salt in this file.
// Yes you can even put the whole fucking bee movie script if you
// wanna make hash generation longer.
const server_salt = await Deno.readTextFile("./serversalt.txt");

export const TimestampToString = (timestamp) => {
	const datetime = (new Date(1000*timestamp)).toISOString()
		.replace(/T/, " ");

	return datetime.substring(0, datetime.length-5);
}
// returns an hex string
function passwordhash(pass: string): string {
	const message = server_salt + pass;
	return sha256checksumSync(new TextEncoder().encode(message));
}
function sha256checksumSync(data: Uint8Array): string {
	const buf = new Uint8Array(crypto.subtle.digestSync("SHA-256", data));
	return Array.from(buf).map((x) => x.toString(16).padStart(2, '0')).join('');
}
async function sha256checksum(data: Uint8Array): string {
	const buf = new Uint8Array(await crypto.subtle.digest("SHA-256", data));
	return Array.from(buf).map((x) => x.toString(16).padStart(2, '0')).join('');
}
// update verifstring
/*
export const randomString = (length: number): string =>
	Array<string>(length).fill().map(
		(_) => verifchars[Math.floor(Math.random() * verifchars.length)]
).join('');
*/
export const randomString = crypto.randomUUID;

export function searchJSON(arr, objName: str, el: any): number {
	for (const i in arr) {
		const obj = arr[i][objName];
		if (obj == el) return i;
	}
	return -1;
}

// SQL query functions
function userExists(username: string): bool {
	const q = db.query("SELECT id FROM users WHERE name=?", [username]);
	return q.length > 0;
}
export function getUserID(username: string): number | null {
	const q = db.query("SELECT id FROM users WHERE name=?", [username]);
	return (q.length > 0) ? q[0][0] : null;
}
export function isUserAdmin(username: string): bool {
	const q = db.query("SELECT admin FROM users WHERE name=?", [username]);
	if (!q) return false;
	return q[0][0] == true;
}
export function isUserBanned(username: string): bool {
	const q = db.query("SELECT banned FROM Users WHERE name=?", [username]);
	if (!q) return false;
	return q[0][0] == true;
}
// admin commands
export function adminBanUser(username: string, ban: bool = true) {
	if (userExists(username) || !isUserAdmin(username)) {
		const banned = isUserBanned(username);
		if ((!banned && ban) ||
			(banned && !ban)) {
			db.query("UPDATE users SET banned=? WHERE name=?",
					 [ban, username]);
		}
	}
}
// invite stuff
export function adminCreateInvite(): string {
	const invite = randomString();

	db.query("INSERT INTO invites (code) VALUES (?)", [invite]);
	return invite;
}
export function adminRevokeInvite(code: string) {
	db.query("DELETE FROM invites WHERE code=?", [code]);
}
function useInviteCode(code: string): bool {
	const query = db.query("SELECT * FROM invites WHERE code=?",
						   [code]);
	if (query.length > 0) {
		adminRevokeInvite(code);
		return true;
	} else return false;
}
export function listInvites(): string[] {
	const query = db.query("SELECT code FROM invites");
	return query.map((x) => x[0]);
}

//// posting stuff
export class KibtyPost {
	id: number;
	board: string;
	author: number;
	timestamp: number;
	content: string;
	img_id: string;
	reply: bool = false;
	replyid: number = null;
	replies: KibtyPost[] = null;
	pinned: bool;
	fromapi: bool;
	deleted: bool;

	getReplies(max: number = 3, offset = 0): void {
		const query = db.query(
			"SELECT * FROM posts WHERE reply=TRUE AND deleted=FALSE " +
			"AND replyid=? ORDER BY stamp LIMIT ? OFFSET ?",
			[this.id, max, offset]);
		this.replies = query.length > 0 ?
			query.map(x => new KibtyPost(x)) : null;
	}

	constructor(row: any[],
				replies: bool = false, maxrep: number = 3) {
		[this.id, this.board, this.author, this.timestamp,
		 this.content, this.img_id, this.reply, this.replyid,
		 this.pinned, this.fromapi, this.deleted] = row;
		if (replies) this.getReplies(maxrep);
	}
}
export class KibtyUser {
	id:       number;
	name:     string;
	bio:      string;
	banned:   bool;
	admin:    bool;
	inviter:  KibtyUser | null;

	constructor(thing: string | number) {
		const ntype = typeof thing == 'string' ? 'name' : 'id';
		let query = null;
		query = db.query("SELECT id, name, bio, banned, admin" +
			`, invited_by FROM users WHERE ${ntype}=?`, [thing]);

		if (query.length) {
			let inviter: number | null; 
			[this.id, this.name, this.bio, this.banned, this.admin,
			 inviter] =
				query[0];
			if (inviter)
				this.inviter = new KibtyUser(inviter);
			this.banned = this.banned == true;
			this.admin  = this.admin == true;
		} else {
			this.id = -1;
			this.name = "[INVALID]";
			this.bio  = "This user does not exist.";
			this.banned = false; this.admin = false;
		}
	}
}
export class KibtyQuery {
	users: Record<number, KibtyUser>;

	gatherIDs() {
		let ids = this.posts.map(x => x.author);
		for (const post of this.posts) {
			const replies = post.replies;
			if (replies && replies.length > 0) {
				ids = ids.concat(replies.map(x => x.author));
			}
		}
		const result = [...new Set(ids)];
		return result;
	}

	constructor(public posts: KibtyPost[]) {
		const userids = this.gatherIDs();
		this.users = {};
		for (const id of userids)
			this.users[id] = new KibtyUser(id);
	}
}
export function getTopPosts(board: string, page: number = 0): KibtyQuery {
	if (page < 0) return null;
	const query = db.query("SELECT * FROM posts WHERE board=? AND " +
		"reply=FALSE AND deleted=FALSE ORDER BY stamp" +
		" DESC LIMIT 10 OFFSET ?", [board, page*10]);
	if (query.length > 0) {
		const posts = query.map(post => new KibtyPost(post, true));
		return new KibtyQuery(posts);
	}
	return null;
}
// if you are kibty, matthilde gives patpat,
// if you are bnuuy, matthilde also gives patpat,,
export function getUserLatestPosts(uid: number): KibtyQuery | null {
	const query = db.query(
		"SELECT * FROM posts WHERE author=? AND reply=FALSE " +
			"AND deleted=FALSE ORDER BY stamp DESC LIMIT 5", [uid]);
	if (query.length > 0) {
		const posts = query.map(post => new KibtyPost(post, false));
		return new KibtyQuery(posts);
	}
	return null;
}
export function getLatestNews() {
	const query = db.query(
		"SELECT * FROM posts WHERE board=? AND reply=FALSE " +
			"AND deleted=FALSE ORDER BY stamp DESC LIMIT 1",
	    [config.newsBoard]);
	if (query.length > 0) {
		const posts = query.map(post => new KibtyPost(post, false));
		return new KibtyQuery(posts);
	}
	return null;
}
export function getPostById(id: number, getReplies: bool = false): KibtyPost {
	const query = db.query("SELECT * FROM posts WHERE id=?", [id]);
	if (query.length > 0) {
		const post = new KibtyPost(query[0], getReplies, -1);
		return post;
	}
	return null;
}

// board stuff

// posting stuff
export async function downloadImage(fn: string, mime: string): string {
	const ext = {
		'image/png': '.png',
		'image/jpeg': '.jpg'}[mime];
	let filename = (await sha256checksum(await Deno.readFile(fn))) + ext;

	if (!(await exists(imgDir + filename)))
		await copy(fn, imgDir + filename);
	await Deno.remove(fn);

	return filename;
}
export function deletePost(id: number) {
	db.query("UPDATE posts SET deleted=TRUE WHERE id=?", [id]);
}
function postSomething(userID: number,
							  board: string,
							  message: string,
							  image: string | null = null,
							  reply?: number | null = null) {
	const curtime = Math.floor(Date.now() / 1000);
	db.query("INSERT INTO posts " +
		"(board, author, stamp, content, img, reply, replyid) " +
		"VALUES (?, ?, ?, ?, ?, ?, ?)",
			 [board, userID, curtime, message, image,
			  reply != null, reply]);
}

// size: bio is too large.
export function changeUserBio(uid: number, bio: string): string {
	if (bio.length > config.posts.maxChars)
		return 'size';
	else {
		const newBio = markup.renderHTML(bio);
		db.query("UPDATE users SET bio=? WHERE id=?", [newBio, uid]);
		return 'success';
	}
}

/*
  message: message is invalid
  image:   image is invalid
  success: successfully posted
*/
export async function userPost(username: string, data, board,
							   reply: number = null): string {
	const supported_types = ['image/png', 'image/jpeg'];

	const message = data.fields['message'];
	let   image   = data.files[0];
	if (!message ||
		message.length == 0 || message.length > config.posts.maxChars)
		return "message";
	else if (!image || supported_types.indexOf(image.contentType) == -1) {
		if (reply && image.originalName == '')
			image = null;
		else
			return "image";
	}

	const userID = getUserID(username);
	let imgName = null;
	if (image)
		imgName = await downloadImage(image.filename,
									  image.contentType);
	if (reply) {
		postSomething(userID,
						board,
						markup.renderHTML(message),
						imgName, reply);
	} else {
		postSomething(userID,
						board,
						markup.renderHTML(message),
						imgName);
	}
	return "success";
}

/*
  creds:   Invalid username or password
  exists:  Account already exists
  code:    Invalid invite/auth code
  banned:  IP is banned (TODO)
  success: Account successfully created
*/
export const uregex = /^[a-z0-9_.]{3,16}$/;
export const pregex = /^[^\s]{8,40}$/;
export function registerAccount(username: string,
								password: string,
								verification: string = null): number {
	// checks username and password
	if (!uregex.test(username) || !pregex.test(password))
		return "creds";
	// checks if user exists
	if (userExists(username))
		return "exists";
	// If no verification string, return null
	if (!verification || !useInviteCode(verification))
		return "code";

	db.query("INSERT INTO users (name, password) VALUES (?, ?)",
			 [username, passwordhash(password)]);
	verifstring = null;

	return "success";
}
/*
  creds:   Invalid username or password
  banned:  Account is banned
  success: Successful login
  */
export function loginAccount(username: string,
							 password: string): string {
	let user = db.query("SELECT name, password, banned FROM users " +
						  "WHERE name=?", [username]);
	if (user.length == 0) return "creds";
	else user = user[0];
	if (passwordhash(password) != user[1]) return "creds";
	else if (user[2]) return "banned";

	return "success";
}
